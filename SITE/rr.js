!function(a, b, c) {
    "function" == typeof define && define.amd ? define([ "jquery" ], function(d) {
        return c(d, a, b), d.mobile;
    }) : c(a.jQuery, a, b);
}(this, document, function(a, b, c, d) {
    !function(a, b, c, d) {
        function e(a) {
            while (a && "undefined" != typeof a.originalEvent) a = a.originalEvent;
            return a;
        }
        function f(b, c) {
            var f = b.type, g, h, i, j, k, l, m, n, o;
            b = a.Event(b), b.type = c, g = b.originalEvent, h = a.event.props, f.search(/^(mouse|click)/) > -1 && (h = C);
            if (g) for (m = h.length, j; m; ) j = h[--m], b[j] = g[j];
            f.search(/mouse(down|up)|click/) > -1 && !b.which && (b.which = 1);
            if (f.search(/^touch/) !== -1) {
                i = e(g), f = i.touches, k = i.changedTouches, l = f && f.length ? f[0] : k && k.length ? k[0] : d;
                if (l) for (n = 0, o = A.length; n < o; n++) j = A[n], b[j] = l[j];
            }
            return b;
        }
        function g(b) {
            var c = {}, d, e;
            while (b) {
                d = a.data(b, x);
                for (e in d) d[e] && (c[e] = c.hasVirtualBinding = !0);
                b = b.parentNode;
            }
            return c;
        }
        function h(b, c) {
            var d;
            while (b) {
                d = a.data(b, x);
                if (d && (!c || d[c])) return b;
                b = b.parentNode;
            }
            return null;
        }
        function i() {
            K = !1;
        }
        function j() {
            K = !0;
        }
        function k() {
            O = 0, I.length = 0, J = !1, j();
        }
        function l() {
            i();
        }
        function m() {
            n(), E = setTimeout(function() {
                E = 0, k();
            }, a.vmouse.resetTimerDuration);
        }
        function n() {
            E && (clearTimeout(E), E = 0);
        }
        function o(b, c, d) {
            var e;
            if (d && d[b] || !d && h(c.target, b)) e = f(c, b), a(c.target).trigger(e);
            return e;
        }
        function p(b) {
            var c = a.data(b.target, y), d;
            !J && (!O || O !== c) && (d = o("v" + b.type, b), d && (d.isDefaultPrevented() && b.preventDefault(), 
            d.isPropagationStopped() && b.stopPropagation(), d.isImmediatePropagationStopped() && b.stopImmediatePropagation()));
        }
        function q(b) {
            var c = e(b).touches, d, f, h;
            c && 1 === c.length && (d = b.target, f = g(d), f.hasVirtualBinding && (O = N++, 
            a.data(d, y, O), n(), l(), H = !1, h = e(b).touches[0], F = h.pageX, G = h.pageY, 
            o("vmouseover", b, f), o("vmousedown", b, f)));
        }
        function r(a) {
            if (K) return;
            H || o("vmousecancel", a, g(a.target)), H = !0, m();
        }
        function s(b) {
            if (K) return;
            var c = e(b).touches[0], d = H, f = a.vmouse.moveDistanceThreshold, h = g(b.target);
            H = H || Math.abs(c.pageX - F) > f || Math.abs(c.pageY - G) > f, H && !d && o("vmousecancel", b, h), 
            o("vmousemove", b, h), m();
        }
        function t(a) {
            if (K) return;
            j();
            var b = g(a.target), c, d;
            o("vmouseup", a, b), H || (c = o("vclick", a, b), c && c.isDefaultPrevented() && (d = e(a).changedTouches[0], 
            I.push({
                touchID: O,
                x: d.clientX,
                y: d.clientY
            }), J = !0)), o("vmouseout", a, b), H = !1, m();
        }
        function u(b) {
            var c = a.data(b, x), d;
            if (c) for (d in c) if (c[d]) return !0;
            return !1;
        }
        function v() {}
        function w(b) {
            var c = b.substr(1);
            return {
                setup: function() {
                    u(this) || a.data(this, x, {});
                    var d = a.data(this, x);
                    d[b] = !0, D[b] = (D[b] || 0) + 1, 1 === D[b] && M.bind(c, p), a(this).bind(c, v), 
                    L && (D.touchstart = (D.touchstart || 0) + 1, 1 === D.touchstart && M.bind("touchstart", q).bind("touchend", t).bind("touchmove", s).bind("scroll", r));
                },
                teardown: function() {
                    --D[b], D[b] || M.unbind(c, p), L && (--D.touchstart, D.touchstart || M.unbind("touchstart", q).unbind("touchmove", s).unbind("touchend", t).unbind("scroll", r));
                    var d = a(this), e = a.data(this, x);
                    e && (e[b] = !1), d.unbind(c, v), u(this) || d.removeData(x);
                }
            };
        }
        var x = "virtualMouseBindings", y = "virtualTouchID", z = "vmouseover vmousedown vmousemove vmouseup vclick vmouseout vmousecancel".split(" "), A = "clientX clientY pageX pageY screenX screenY".split(" "), B = a.event.mouseHooks ? a.event.mouseHooks.props : [], C = a.event.props.concat(B), D = {}, E = 0, F = 0, G = 0, H = !1, I = [], J = !1, K = !1, L = "addEventListener" in c, M = a(c), N = 1, O = 0, P, Q;
        a.vmouse = {
            moveDistanceThreshold: 10,
            clickDistanceThreshold: 10,
            resetTimerDuration: 1500
        };
        for (Q = 0; Q < z.length; Q++) a.event.special[z[Q]] = w(z[Q]);
        L && c.addEventListener("click", function(b) {
            var c = I.length, d = b.target, e, f, g, h, i, j;
            if (c) {
                e = b.clientX, f = b.clientY, P = a.vmouse.clickDistanceThreshold, g = d;
                while (g) {
                    for (h = 0; h < c; h++) {
                        i = I[h], j = 0;
                        if (g === d && Math.abs(i.x - e) < P && Math.abs(i.y - f) < P || a.data(g, y) === i.touchID) {
                            b.preventDefault(), b.stopPropagation();
                            return;
                        }
                    }
                    g = g.parentNode;
                }
            }
        }, !0);
    }(a, b, c), function(a) {
        a.mobile = {};
    }(a), function(a, b) {
        var d = {
            touch: "ontouchend" in c
        };
        a.mobile.support = a.mobile.support || {}, a.extend(a.support, d), a.extend(a.mobile.support, d);
    }(a), function(a, b, d) {
        function e(b, c, e, f) {
            var g = e.type;
            e.type = c, f ? a.event.trigger(e, d, b) : a.event.dispatch.call(b, e), e.type = g;
        }
        var f = a(c), g = a.mobile.support.touch, h = "touchmove scroll", i = g ? "touchstart" : "mousedown", j = g ? "touchend" : "mouseup", k = g ? "touchmove" : "mousemove";
        a.each("touchstart touchmove touchend tap taphold swipe swipeleft swiperight scrollstart scrollstop".split(" "), function(b, c) {
            a.fn[c] = function(a) {
                return a ? this.bind(c, a) : this.trigger(c);
            }, a.attrFn && (a.attrFn[c] = !0);
        }), a.event.special.scrollstart = {
            enabled: !0,
            setup: function() {
                function b(a, b) {
                    f = b, e(c, f ? "scrollstart" : "scrollstop", a);
                }
                var c = this, d = a(c), f, g;
                d.bind(h, function(c) {
                    if (!a.event.special.scrollstart.enabled) return;
                    f || b(c, !0), clearTimeout(g), g = setTimeout(function() {
                        b(c, !1);
                    }, 50);
                });
            },
            teardown: function() {
                a(this).unbind(h);
            }
        }, a.event.special.tap = {
            tapholdThreshold: 750,
            emitTapOnTaphold: !0,
            setup: function() {
                var b = this, c = a(b), d = !1;
                c.bind("vmousedown", function(g) {
                    function h() {
                        clearTimeout(l);
                    }
                    function i() {
                        h(), c.unbind("vclick", j).unbind("vmouseup", h), f.unbind("vmousecancel", i);
                    }
                    function j(a) {
                        i(), !d && k === a.target ? e(b, "tap", a) : d && a.preventDefault();
                    }
                    d = !1;
                    if (g.which && 1 !== g.which) return !1;
                    var k = g.target, l;
                    c.bind("vmouseup", h).bind("vclick", j), f.bind("vmousecancel", i), l = setTimeout(function() {
                        a.event.special.tap.emitTapOnTaphold || (d = !0), e(b, "taphold", a.Event("taphold", {
                            target: k
                        }));
                    }, a.event.special.tap.tapholdThreshold);
                });
            },
            teardown: function() {
                a(this).unbind("vmousedown").unbind("vclick").unbind("vmouseup"), f.unbind("vmousecancel");
            }
        }, a.event.special.swipe = {
            scrollSupressionThreshold: 30,
            durationThreshold: 1e3,
            horizontalDistanceThreshold: 30,
            verticalDistanceThreshold: 30,
            getLocation: function(a) {
                var c = b.pageXOffset, d = b.pageYOffset, e = a.clientX, f = a.clientY;
                if (0 === a.pageY && Math.floor(f) > Math.floor(a.pageY) || 0 === a.pageX && Math.floor(e) > Math.floor(a.pageX)) e -= c, 
                f -= d; else if (f < a.pageY - d || e < a.pageX - c) e = a.pageX - c, f = a.pageY - d;
                return {
                    x: e,
                    y: f
                };
            },
            start: function(b) {
                var c = b.originalEvent.touches ? b.originalEvent.touches[0] : b, d = a.event.special.swipe.getLocation(c);
                return {
                    time: new Date().getTime(),
                    coords: [ d.x, d.y ],
                    origin: a(b.target)
                };
            },
            stop: function(b) {
                var c = b.originalEvent.touches ? b.originalEvent.touches[0] : b, d = a.event.special.swipe.getLocation(c);
                return {
                    time: new Date().getTime(),
                    coords: [ d.x, d.y ]
                };
            },
            handleSwipe: function(b, c, d, f) {
                if (c.time - b.time < a.event.special.swipe.durationThreshold && Math.abs(b.coords[0] - c.coords[0]) > a.event.special.swipe.horizontalDistanceThreshold && Math.abs(b.coords[1] - c.coords[1]) < a.event.special.swipe.verticalDistanceThreshold) {
                    var g = b.coords[0] > c.coords[0] ? "swipeleft" : "swiperight";
                    return e(d, "swipe", a.Event("swipe", {
                        target: f,
                        swipestart: b,
                        swipestop: c
                    }), !0), e(d, g, a.Event(g, {
                        target: f,
                        swipestart: b,
                        swipestop: c
                    }), !0), !0;
                }
                return !1;
            },
            eventInProgress: !1,
            setup: function() {
                var b, c = this, d = a(c), e = {};
                b = a.data(this, "mobile-events"), b || (b = {
                    length: 0
                }, a.data(this, "mobile-events", b)), b.length++, b.swipe = e, e.start = function(b) {
                    if (a.event.special.swipe.eventInProgress) return;
                    a.event.special.swipe.eventInProgress = !0;
                    var d, g = a.event.special.swipe.start(b), h = b.target, i = !1;
                    e.move = function(b) {
                        if (!g || b.isDefaultPrevented()) return;
                        d = a.event.special.swipe.stop(b), i || (i = a.event.special.swipe.handleSwipe(g, d, c, h), 
                        i && (a.event.special.swipe.eventInProgress = !1)), Math.abs(g.coords[0] - d.coords[0]) > a.event.special.swipe.scrollSupressionThreshold && b.preventDefault();
                    }, e.stop = function() {
                        i = !0, a.event.special.swipe.eventInProgress = !1, f.off(k, e.move), e.move = null;
                    }, f.on(k, e.move).one(j, e.stop);
                }, d.on(i, e.start);
            },
            teardown: function() {
                var b, c;
                b = a.data(this, "mobile-events"), b && (c = b.swipe, delete b.swipe, b.length--, 
                0 === b.length && a.removeData(this, "mobile-events")), c && (c.start && a(this).off(i, c.start), 
                c.move && f.off(k, c.move), c.stop && f.off(j, c.stop));
            }
        }, a.each({
            scrollstop: "scrollstart",
            taphold: "tap",
            swipeleft: "swipe.left",
            swiperight: "swipe.right"
        }, function(b, c) {
            a.event.special[b] = {
                setup: function() {
                    a(this).bind(c, a.noop);
                },
                teardown: function() {
                    a(this).unbind(c);
                }
            };
        });
    }(a, this);
});